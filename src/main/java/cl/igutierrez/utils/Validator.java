package cl.igutierrez.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Service;

@Service
public class Validator {
	 private static final String PASSWORD_PATTERN =
	            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,15})";

	    private static final Pattern pattern = Pattern.compile(PASSWORD_PATTERN);

	    public  boolean isValid( String password) {
	        Matcher matcher = pattern.matcher(password);

	        return matcher.matches();
	    }
	    
	    public boolean isEmailValid(String email) {
	    	return  EmailValidator.getInstance().isValid(email);
	
	    }
}
