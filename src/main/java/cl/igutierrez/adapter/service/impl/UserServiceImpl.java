package cl.igutierrez.adapter.service.impl;

import java.util.Date;
import java.util.List;

import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;


import cl.igutierrez.adapter.entity.UserEntity;
import cl.igutierrez.adapter.repository.UserRepository;
import cl.igutierrez.adapter.service.UserService;
import cl.igutierrez.dto.LoginDto;
import cl.igutierrez.dto.UserCreateRequestDto;
import cl.igutierrez.exception.EmailExistException;
import cl.igutierrez.exception.LoginException;
import cl.igutierrez.exception.PasswordException;
import cl.igutierrez.utils.Validator;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
@Service
@Transactional
@RequiredArgsConstructor
public class UserServiceImpl implements UserService{
	
	private final UserRepository userRepository;
	private final Validator passwordValidator;

	@Override
	public UserEntity crearUsuario(UserCreateRequestDto user)  {
	
		if(userRepository.existsByEmail(user.getEmail())) {
			throw new EmailExistException("El Email Ingresado ya existe");
		}
		if(!passwordValidator.isEmailValid(user.getEmail())) {
			throw new PasswordException("El Email No es Valido");
		}
		if(!passwordValidator.isValid(user.getPassword())) {
			throw new PasswordException("La Password debe Tener una Mayuscula y un numero");
		}
	
		return guardaUsuario(user);

	}
	
	
	public UserEntity guardaUsuario(UserCreateRequestDto user){
		System.out.println(user);
		
		UserEntity entity=new UserEntity();
		entity.setPassword(user.getPassword());
		entity.setEmail(user.getEmail());
		entity.setName(user.getName());
		entity.setToken(creaToken(user.getEmail()));

		entity.setphone(user.getPhones());

		UserEntity response =userRepository.saveAndFlush(entity);
		System.out.println(response.getPhones().get(1).getUser().getId_user());
		return response;
	}


	public String creaToken(String username){
		String token = getJWTToken(username);	
		return token;
	}

	
	public UserEntity logearUsuario(LoginDto dto) {
		UserEntity user =userRepository.findByEmailAndPassword(dto.getEmail(), dto.getPassword());
		if(user != null) {
			user.setToken(creaToken(user.getEmail()));
			user.setLast_login(new Date());

			return userRepository.save(user); 
		}else {
			throw new LoginException("El Email o la clave es incorrecta");
		}

		
	}
	
	
	private String getJWTToken(String username) {
		String secretKey = "keyigutierrez";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ROLE_USER");
		
		String token = Jwts
				.builder()
				.setId("igutierrez")
				.setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS256,
						secretKey.getBytes()).compact();

		return "Bearer " + token;

		
	}
}
