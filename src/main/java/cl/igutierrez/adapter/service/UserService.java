package cl.igutierrez.adapter.service;

import cl.igutierrez.adapter.entity.UserEntity;
import cl.igutierrez.dto.LoginDto;
import cl.igutierrez.dto.UserCreateRequestDto;
import jakarta.transaction.Transactional;



@Transactional
public interface UserService {

	UserEntity crearUsuario(UserCreateRequestDto user) ;
	
	UserEntity logearUsuario(LoginDto dto);
}
