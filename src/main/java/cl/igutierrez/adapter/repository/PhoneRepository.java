package cl.igutierrez.adapter.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import cl.igutierrez.adapter.entity.PhoneEntity;
import jakarta.transaction.Transactional;

@Transactional
public interface PhoneRepository extends JpaRepository<PhoneEntity, Integer>  {


}
