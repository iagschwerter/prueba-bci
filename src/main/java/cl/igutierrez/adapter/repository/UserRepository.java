package cl.igutierrez.adapter.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.igutierrez.adapter.entity.UserEntity;
import jakarta.transaction.Transactional;
@Repository
@Transactional
public interface UserRepository extends JpaRepository<UserEntity, Integer> {
	Boolean existsByEmail(String email);
	UserEntity findByEmailAndPassword(String email, String password);
	UserEntity findByEmail(String email);
	
}
