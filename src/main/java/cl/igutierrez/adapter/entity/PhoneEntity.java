package cl.igutierrez.adapter.entity;



import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.Table;
import lombok.Data;
@Data
@Entity
@Table(name = "phone_usuario")
public class PhoneEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id_Phone;
	
	@JsonBackReference
	@MapsId("id_user")
	@ManyToOne(fetch = FetchType.LAZY)
	private UserEntity user; 
	
	private Integer number;
	private String citycode;
	private String countrycode;
}
