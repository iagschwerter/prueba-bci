package cl.igutierrez.adapter.entity;

import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import java.util.UUID;

import org.hibernate.annotations.CreationTimestamp;


import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.UuidGenerator;


import com.fasterxml.jackson.annotation.JsonManagedReference;


import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

import jakarta.persistence.OneToMany;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;
@Data
@Entity
@Table(name ="usuario")
public class UserEntity {
	
	@Id
	@GeneratedValue
	@UuidGenerator
    @Column(name = "id_user",updatable = false, nullable = false)
	private UUID id_user;
	
	private String name;

    @Column(name = "email", unique = true)
	private String email;

	private String password;
	
	@JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "user" , orphanRemoval = true)
    private List<PhoneEntity> phones = new ArrayList<>();
    
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date created = new Date();

	@UpdateTimestamp
	private Date modified = new Date();
	
	private Date last_login;
	private Boolean isactive;
	private String token;
	
	 @PrePersist
	 private void preCarga() {
		 this.last_login= new Date();
		 this.isactive=true;
	 }


	   public void setphone(List<PhoneEntity> phonesList) {
		    this.phones = phonesList;
		    this.phones.forEach(phone -> phone.setUser(this));
		}
}
