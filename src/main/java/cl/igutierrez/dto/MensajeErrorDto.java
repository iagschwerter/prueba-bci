package cl.igutierrez.dto;



import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Generated
@Getter
@Setter
public class MensajeErrorDto {

	private String mensaje;
}
