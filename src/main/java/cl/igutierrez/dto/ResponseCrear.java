package cl.igutierrez.dto;

import java.util.Date;

import java.util.UUID;


import cl.igutierrez.adapter.entity.UserEntity;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Generated
@Getter
@Setter
public class ResponseCrear {
	private UUID id;
	private Date created;
	private Date modified;
	private Date last_login;
	private String token;
	private Boolean isActive;

	public ResponseCrear(UserEntity entity){
		this.id=entity.getId_user();
		this.created=entity.getCreated();
		this.modified=entity.getModified();
		this.last_login=entity.getLast_login();
		this.token=entity.getToken();
		this.isActive=entity.getIsactive();
		 
	 }
}
