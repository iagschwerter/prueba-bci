package cl.igutierrez.dto;


import java.util.List;


import cl.igutierrez.adapter.entity.PhoneEntity;

import lombok.Getter;

import lombok.Setter;

@Getter
@Setter
public class UserCreateRequestDto {
	
	private String name;


    private String email;

    private String password;
    private List< PhoneEntity> phones;
}
