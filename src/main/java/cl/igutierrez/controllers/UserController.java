package cl.igutierrez.controllers;



import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import cl.igutierrez.adapter.service.UserService;
import cl.igutierrez.dto.LoginDto;
import cl.igutierrez.dto.ResponseCrear;
import cl.igutierrez.dto.UserCreateRequestDto;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/v1/user")
@RequiredArgsConstructor
public class UserController {
	
	private final UserService userService;
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(value = "/crear", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseCrear crear( @RequestBody UserCreateRequestDto dto ) {

		return new ResponseCrear(userService.crearUsuario(dto));
	}
	
	@ResponseStatus(HttpStatus.OK)
	@PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseCrear login( @RequestBody LoginDto dto ) throws Exception {
		
		return new ResponseCrear(userService.logearUsuario(dto));
		
	}

}
