package cl.igutierrez.controllers;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import cl.igutierrez.dto.MensajeErrorDto;
import cl.igutierrez.exception.EmailExistException;
import cl.igutierrez.exception.LoginException;
import cl.igutierrez.exception.PasswordException;

@RestControllerAdvice
public class ExceptionHandlerController {
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value=EmailExistException.class)
	public MensajeErrorDto runtimeExceptionEmail(EmailExistException ex){
		MensajeErrorDto mensaje=new MensajeErrorDto();
		mensaje.setMensaje(ex.getMessage());
		return mensaje;
		
	}
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value=PasswordException.class)
	public MensajeErrorDto runtimeExceptionPassword(PasswordException ex){
		MensajeErrorDto mensaje=new MensajeErrorDto();
		mensaje.setMensaje(ex.getMessage());
		return mensaje;
		
	}
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@ExceptionHandler(value=LoginException.class)
	public MensajeErrorDto runtimeExceptionLogin(LoginException ex){
		MensajeErrorDto mensaje=new MensajeErrorDto();
		mensaje.setMensaje(ex.getMessage());
		return mensaje;
		
	}
	
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value=RuntimeException.class)
	public MensajeErrorDto runtimeExceptionGeneral(RuntimeException ex){
		MensajeErrorDto mensaje=new MensajeErrorDto();
		mensaje.setMensaje("Error al ejecutar su Solicitud");
		return mensaje;
		
	}
}
