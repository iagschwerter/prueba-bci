package cl.igutierrez.exception;

public class PasswordException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6086514951073907938L; 
	public PasswordException(String errorMessage) {
        super(errorMessage);
    }
}
