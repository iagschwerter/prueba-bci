package cl.igutierrez.exception;

public class EmailExistException extends RuntimeException { 

	/**
	 * 
	 */
	private static final long serialVersionUID = -2790954948396196939L;

	public EmailExistException(String errorMessage) {
        super(errorMessage);
    }
}
