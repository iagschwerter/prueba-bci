package cl.igutierrez.exception;

public class LoginException extends RuntimeException {


	/**
	 * 
	 */
	private static final long serialVersionUID = -1005735169889386614L;

	public LoginException(String errorMessage) {
        super(errorMessage);
    }

}
