package cl.igutierrez.service;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import org.springframework.test.context.junit4.SpringRunner;

import cl.igutierrez.adapter.entity.PhoneEntity;
import cl.igutierrez.adapter.entity.UserEntity;
import cl.igutierrez.adapter.repository.PhoneRepository;
import cl.igutierrez.adapter.repository.UserRepository;
import cl.igutierrez.adapter.service.impl.UserServiceImpl;
import cl.igutierrez.dto.LoginDto;
import cl.igutierrez.dto.UserCreateRequestDto;
import cl.igutierrez.exception.EmailExistException;
import cl.igutierrez.exception.PasswordException;
import cl.igutierrez.utils.Validator;


@RunWith(SpringRunner.class)
public class UserServiceImplTest {
	private UserServiceImpl userService;
	@Mock
	private  UserRepository userRepository;
	@Mock
	private  Validator passwordValidator;
	@Mock
	private  PhoneRepository phoneRepository;

	@Before
	public void setup() {
		userService= new UserServiceImpl(userRepository,passwordValidator);
		
	}
	
	@Test
	public void crearUsuario()  {
		UserEntity response=new UserEntity();
	      UUID id = UUID.fromString("e4fdfc00-c04f-48f1-a6f0-f9cb3a2a7bf8");
		response.setId_user(id);
		response.setCreated(new Date());
		response.setPassword("jUan_17");
		response.setModified(new Date());
		response.setLast_login(new Date());
		response.setToken("Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJpZ3V0aWVycmV6Iiwic3ViIjoiMzIzNDJAZ21haWw");
		when(userRepository.existsByEmail(any())).thenReturn(false);
		when(passwordValidator.isValid(any())).thenReturn(true);
		when(userRepository.saveAndFlush(any())).thenReturn(response);
		UserCreateRequestDto user = new UserCreateRequestDto();
		user.setEmail("email@gmail.com");
		user.setPassword("Juan_25");
		user.setPhones(new ArrayList< PhoneEntity>());
		UserEntity usuario=userService.crearUsuario(user);
	
		assertNotNull(usuario);
	}
	
	
	 @Test(expected = EmailExistException.class)
	public void crearUsuarioExisteEmail()  {

		when(userRepository.existsByEmail(any())).thenReturn(true);

		UserCreateRequestDto user = new UserCreateRequestDto();
		user.setEmail("email@gmail.com");
		user.setPassword("Juan_25");
		user.setPhones(new ArrayList< PhoneEntity>());
		userService.crearUsuario(user);
	

	}
	 
	 @Test(expected = PasswordException.class)
	public void crearUsuarioMalPassword()  {

		when(userRepository.existsByEmail(any())).thenReturn(false);
		when(passwordValidator.isValid(any())).thenReturn(false);
		UserCreateRequestDto user = new UserCreateRequestDto();
		user.setEmail("email@gmail.com");
		user.setPassword("Juan_25");
		user.setPhones(new ArrayList< PhoneEntity>());
		userService.crearUsuario(user);
	

	}
	
	@Test
	public void loginUsuario()  {
		UserEntity response=new UserEntity();
	      UUID id = UUID.fromString("e4fdfc00-c04f-48f1-a6f0-f9cb3a2a7bf8");
		response.setId_user(id);
		response.setCreated(new Date());
		response.setPassword("jUan_17");
		response.setModified(new Date());
		response.setLast_login(new Date());
		response.setToken("Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJpZ3V0aWVycmV6Iiwic3ViIjoiMzIzNDJAZ21haWw");
		
		LoginDto dto= new LoginDto();
		when(userRepository.findByEmailAndPassword(any(),any())).thenReturn(response);

		when(userRepository.save(any())).thenReturn(response);

		UserEntity usuario=userService.logearUsuario(dto);
	
		assertNotNull(usuario);
	}

}
