package cl.igutierrez.controller;



import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;


import static org.mockito.Mockito.*;


import java.util.Date;
import java.util.UUID;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import cl.igutierrez.adapter.entity.UserEntity;
import cl.igutierrez.adapter.service.UserService;
import cl.igutierrez.controllers.UserController;
import cl.igutierrez.dto.LoginDto;


@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class UserControllerTest {
	private MockMvc mvc;
	@InjectMocks
	private UserController userController;
	
	@Mock
	private UserService userService;
	
	@Before
	public void setup() {
		userController = new UserController(userService);
		this.mvc =MockMvcBuilders.standaloneSetup(userController).build();
	}
	
	@Test
	public void crear() throws Exception {
		UserEntity response=new UserEntity();
	      UUID id = UUID.fromString("e4fdfc00-c04f-48f1-a6f0-f9cb3a2a7bf8");
		response.setId_user(id);
		response.setCreated(new Date());
		response.setPassword("jUan_17");
		response.setModified(new Date());
		response.setLast_login(new Date());
		response.setToken("Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJpZ3V0aWVycmV6Iiwic3ViIjoiMzIzNDJAZ21haWw");
		response.setEmail("email@test.cl");
		response.setIsactive(true);
		when(userService.crearUsuario(any())).thenReturn(response);
		

		
		ResultActions result= mvc.perform( MockMvcRequestBuilders
			      .post("/v1/user/crear")
			      .content(asJsonString(response))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON));
		result.andExpect(MockMvcResultMatchers.status().isCreated());
				
		
	}
	

	
	@Test
	public void login() throws Exception {
		LoginDto logdto=new LoginDto();
		UserEntity response=new UserEntity();
	      UUID id = UUID.fromString("e4fdfc00-c04f-48f1-a6f0-f9cb3a2a7bf8");
		response.setId_user(id);
		response.setCreated(new Date());
		response.setPassword("jUan_17");
		response.setModified(new Date());
		response.setLast_login(new Date());
		response.setToken("Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJpZ3V0aWVycmV6Iiwic3ViIjoiMzIzNDJAZ21haWw");
		response.setEmail("email@test.cl");
		response.setIsactive(true);
		when(userService.logearUsuario((any()))).thenReturn(response);
		

		
		ResultActions result= mvc.perform( MockMvcRequestBuilders
			      .post("/v1/user/login")
			      .content(asJsonString(logdto))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON));
		result.andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	
	public static String asJsonString(final Object obj) {
	    try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}

}
